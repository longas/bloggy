Blog con Django
===============
Gabriel Longás
--------------

### bugs / Cosas por terminar
* Arreglar el subir imágenes
* Poner CSS

### Instalación y configuración
* Instalamos Django (**$ sudo pip install django**)
* Instalamos South (**$ sudo pip install south**)
* Ejecutamos el servidor (**$ python manage.py runserver**)
* Entramos a nuestro navegador y nos metemos en **localhost:8000**