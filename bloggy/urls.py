from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

urlpatterns = patterns('',
  url(r'^admin/', include(admin.site.urls)),
  url(r'^blog/$', 'blog.views.index'), (r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': os.path.join(BASE_DIR, '/blog/static/media')}),
  url(r'^blog/(?P<post_id>\d+)/$', 'blog.views.post'),
)